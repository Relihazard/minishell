/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   exit.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/25 00:04:00 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/09 12:06:18 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int		ft_exit(char **argv)
{
	int		ret;

	ret = argv[1] ? ft_atoi(argv[1]) : 0;
	ft_memdel2d((void **)argv);
	ft_memdel2d((void **)g_environ);
	exit(ret);
	return (ret);
}
