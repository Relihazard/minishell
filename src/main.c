/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/20 12:15:03 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/19 22:45:30 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char	**g_environ = NULL;
int		g_ret = 0;
int		g_errno = 0;

static void	ft_exec_list(char **list)
{
	char	**args;
	char	*cmd;
	int		i;

	i = 0;
	while (list[i])
	{
		cmd = ft_strtrim(list[i++]);
		args = ft_strsplitspace(cmd);
		ft_strdel(&cmd);
		if (*args)
			g_ret = ft_builtin(args);
		ft_memdel2d((void **)args);
	}
	ft_memdel2d((void **)list);
}

int			main(int argc, char **argv, char **environ)
{
	char	*cmd;
	int		gnl_ret;

	(void)argc;
	ft_initenv(environ);
	while (42)
	{
		signal(SIGINT, &ft_signal_handler_shell);
		(void)ft_printf(PROMPT);
		gnl_ret = get_next_line(0, &cmd);
		if (gnl_ret && *cmd)
		{
			argv = ft_strsplit(cmd, ';');
			ft_strdel(&cmd);
			ft_exec_list(argv);
		}
		ft_strdel(&cmd);
	}
	ft_memdel2d((void **)g_environ);
	return (0);
}
