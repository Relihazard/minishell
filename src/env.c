/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/22 13:47:00 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/12 17:01:31 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

char		*ft_getenv(char *var)
{
	char	**tmp;
	size_t	len;

	tmp = g_environ;
	len = ft_strlen(var);
	while (tmp && *tmp)
	{
		if (!ft_strncmp(*tmp, var, len))
			return ((*tmp + len + 1));
		tmp++;
	}
	return (NULL);
}

static char	**ft_envdup(char **environ)
{
	size_t	len;
	char	*shlvl;
	char	**new_env;
	size_t	i;

	len = 0;
	while (environ[len])
		len++;
	if (!(new_env = (char **)ft_memalloc(sizeof(char *) * (len + 1))))
		return (NULL);
	i = 0;
	while (environ[i])
	{
		if (!ft_strncmp(environ[i], "SHLVL", 5))
		{
			shlvl = ft_itoa(ft_atoi(environ[i] + 6) + 1);
			new_env[i] = ft_strjoin("SHLVL=", shlvl);
			ft_strdel(&shlvl);
		}
		else
			new_env[i] = ft_strdup(environ[i]);
		i++;
	}
	return (new_env);
}

char		**ft_envrealloc(size_t new_size)
{
	char	**new;
	size_t	i;

	new = (char **)ft_memalloc(sizeof(char *) * (new_size + 1));
	i = 0;
	while (g_environ[i] && i < new_size)
	{
		new[i] = ft_strdup(g_environ[i]);
		ft_strdel(&g_environ[i++]);
	}
	free(g_environ);
	return (new);
}

void		ft_initenv(char **environ)
{
	char	*cwd;
	char	*argv[4];

	if (!(g_environ = ft_envdup(environ)))
		exit(E_NOMEM);
	cwd = NULL;
	cwd = getcwd(cwd, 0);
	argv[0] = "setenv";
	argv[2] = cwd;
	argv[3] = NULL;
	if (!ft_getenv("PWD"))
	{
		argv[1] = "PWD";
		(void)ft_setenv(argv);
	}
	if (!ft_getenv("OLDPWD"))
	{
		argv[1] = "OLDPWD";
		(void)ft_setenv(argv);
	}
	ft_strdel(&cwd);
}

int			ft_env(char **argv)
{
	char	**tmp;

	(void)argv;
	tmp = g_environ;
	while (*tmp)
	{
		(void)ft_printf("%s\n", *tmp);
		tmp++;
	}
	return (0);
}
