# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/12 15:41:40 by agrossma          #+#    #+#              #
#    Updated: 2018/04/19 13:37:11 by agrossma         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

SHELL		:= /bin/bash

#### Start of system configuration section ####

NAME		:= minishell
CC			:= gcc
CFLAGS		+= -Wall -Wextra -Werror
MKDIR		:= mkdir -p
RM			:= rm -f
RMDIR		:= rmdir
ECHO		:= echo
MAKE		:= make
QUIET		:= @

#### End of system configuration section ####

#### Start of files definition section ####

_INCLUDE	:= include/
CFLAGS		+= -I$(_INCLUDE) -Ilibft/$(_INCLUDE)
_SRC		:= src/
SRC			:= \
	main.c \
	env.c \
	error.c \
	cd.c \
	setenv.c \
	unsetenv.c \
	exit.c \
	echo.c \
	execute.c \
	signal.c
_OBJ		:= obj/
OBJ			+= $(addprefix $(_OBJ), $(SRC:.c=.o))

#### End of files definition section ####

#### Start of linking configuration section ####

LD			:= gcc
LDFLAGS		:=
LDLIBS		:= -Llibft -lft

#### End of linking configuration section ####

#### Start of rules definition section ####

.PHONY: all clean fclean re

all: $(NAME)

$(NAME): libft/libft.a $(_OBJ) $(OBJ)
	$(QUIET)$(ECHO) "LD	$@"
	$(QUIET)$(LD) $(OBJ) $(LDFLAGS) $(LDLIBS) -o $@

libft/libft.a:
	$(QUIET)$(MAKE) -C libft/

$(_OBJ):
	$(QUIET)$(MKDIR) $(_OBJ)

$(_OBJ)%.o: $(_SRC)%.c
	$(QUIET)$(ECHO) "CC	$(notdir $@)"
	$(QUIET)$(CC) $(CFLAGS) -c $< -o $@

clean:
	$(QUIET)$(MAKE) -C libft/ clean
	$(QUIET)$(ECHO) "RM	$(_OBJ)"
	$(QUIET)$(RM) $(OBJ)
	$(QUIET)if [ -d "$(_OBJ)" ]; then \
		$(RMDIR) $(_OBJ); \
	fi

fclean: clean
	$(QUIET)$(MAKE) -C libft/ fclean
	$(QUIET)$(ECHO) "RM	$(NAME)"
	$(QUIET)$(RM) $(NAME)

re: fclean all

#### End of rules definition section ####

