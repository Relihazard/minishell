/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   setenv.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/01 00:38:05 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/19 13:41:23 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_varpos(char *var)
{
	int		i;

	i = 0;
	while (g_environ[i])
	{
		if (!ft_strncmp(g_environ[i], var, ft_strlen(var)))
			return (i);
		i++;
	}
	return (-1);
}

static void	ft_envadd(char *name, char *value)
{
	size_t	size;
	char	*join;

	size = ft_2darraylen((void **)g_environ);
	g_environ = ft_envrealloc(size + 1);
	join = ft_strjoin(name, "=");
	g_environ[size] = ft_strjoin(join, value);
	ft_strdel(&join);
}

int			ft_setenv(char **argv)
{
	int		pos;
	char	*join;

	if (ft_2darraylen((void **)argv) == 1)
		return (ft_env(argv));
	else if (ft_2darraylen((void **)argv) > 3)
	{
		g_errno = E_2BIG;
		ft_perror("setenv");
		return (E_2BIG);
	}
	else
	{
		pos = ft_varpos(argv[1]);
		if (pos >= 0)
		{
			ft_strdel(&g_environ[pos]);
			join = ft_strjoin(argv[1], "=");
			g_environ[pos] = ft_strjoin(join, argv[2] ? argv[2] : "\0");
			ft_strdel(&join);
		}
		else
			ft_envadd(argv[1], argv[2] ? argv[2] : "\0");
		return (0);
	}
}
