/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   signal.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/18 23:06:03 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/19 00:21:12 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_signal_handler_cmd(int signo)
{
	if (signo == SIGINT)
	{
		(void)ft_printf("\n");
		signal(signo, &ft_signal_handler_cmd);
	}
}

void	ft_signal_handler_shell(int signo)
{
	if (signo == SIGINT)
	{
		(void)ft_printf("\n"PROMPT);
		signal(signo, &ft_signal_handler_shell);
	}
}
