/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cd.c                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 17:14:24 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/16 19:49:17 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static void	ft_setenv_cd(char *old, char *new)
{
	char	*args[4];

	args[0] = "setenv";
	args[1] = "OLDPWD";
	args[2] = old;
	args[3] = NULL;
	(void)ft_builtin(args);
	args[1] = "PWD";
	args[2] = new;
	(void)ft_builtin(args);
}

static void	ft_change_dir(char *dir)
{
	char	*new_dir;
	char	*old_pwd;

	old_pwd = NULL;
	if (dir == NULL)
		return ;
	old_pwd = getcwd(old_pwd, 0);
	new_dir = ft_strdup(dir);
	if (!chdir(new_dir))
		ft_setenv_cd(old_pwd, new_dir);
	else
	{
		if (access(new_dir, F_OK))
			g_errno = E_NOENT;
		else
		{
			if (access(new_dir, R_OK))
				g_errno = E_ACCESS;
			else
				g_errno = E_NOTDIR;
		}
		ft_perror("cd");
	}
	ft_strdel(&old_pwd);
	ft_strdel(&new_dir);
}

int			ft_cd(char **argv)
{
	size_t	len;

	len = ft_2darraylen((void **)argv);
	if (len == 1)
		ft_change_dir(ft_getenv("HOME"));
	else if (len > 2)
	{
		g_errno = E_2BIG;
		ft_perror("cd");
		return (E_2BIG);
	}
	else
	{
		if (!ft_strncmp(argv[1], "-", 1))
			ft_change_dir(ft_getenv("OLDPWD"));
		else
			ft_change_dir(argv[1]);
	}
	return (0);
}
