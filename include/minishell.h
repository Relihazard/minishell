/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/19 17:52:48 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/19 13:35:15 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

/*
** Includes for authorized functions
** libft.h for the libft functions
** stdlib.h for malloc(3), free(3) and exit(3)
** unistd.h for access(2), close(2), read(2), write(2), getcwd(3). chdir(2)
** fork(2) and execve(2)
** fcntl.h for open(2)
** dirent.h for opendir(2), readdir(2) and closedir(2)
** sys/stat.h for stat(2), lstat(2) and fstat(2)
** sys/wait.h for wait(2), waitpid(2), wait3(2) and wait4(2)
** signal.h for signal(3) and kill(2)
*/
# include <libft.h>
# include <stdlib.h>
# include <unistd.h>
# include <fcntl.h>
# include <dirent.h>
# include <sys/stat.h>
# include <sys/wait.h>
# include <signal.h>

# define E_NOMEM		1
# define E_NOENT		2
# define E_ACCESS		3
# define E_2BIG			4
# define E_NOEXEC		5
# define E_NOTDIR		6

# define PROG_NAME		"minishell"

# define PROMPT			"$> "

extern char	**g_environ;
extern int	g_ret;
extern int	g_errno;

int				ft_env(char **argv);
char			*ft_getenv(char *var);
void			ft_initenv(char **environ);
char			**ft_envrealloc(size_t size);

int				ft_builtin(char **argv);
int				ft_cd(char **argv);
int				ft_echo(char **argv);
int				ft_env(char **argv);
int				ft_exit(char **argv);
int				ft_setenv(char **argv);
int				ft_unsetenv(char **argv);

void			ft_perror(char *cmd);

void			ft_signal_handler_shell(int signo);
void			ft_signal_handler_cmd(int signo);

#endif
