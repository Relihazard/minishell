/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   echo.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/03/26 17:13:53 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/16 18:12:21 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

int			ft_echo(char **argv)
{
	int		i;
	int		has_out;

	i = 1;
	has_out = 0;
	while (argv[i])
	{
		if (has_out)
			(void)write(1, " ", 1);
		if (argv[i][0] == '$')
		{
			has_out = ft_printf("%s",
				ft_getenv(argv[i] + 1) ? ft_getenv(argv[i] + 1) : argv[i]);
			i++;
			continue ;
		}
		has_out = ft_printf("%s", argv[i]);
		i++;
	}
	(void)write(1, "\n", 1);
	return (0);
}
