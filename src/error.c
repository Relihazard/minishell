/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/12 13:52:34 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/16 19:40:51 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

void	ft_perror(char *cmd)
{
	ft_putstr_fd(PROG_NAME": ", 2);
	ft_putstr_fd(cmd, 2);
	ft_putstr_fd(": ", 2);
	g_errno == E_NOENT ? ft_putendl_fd("no such file or directory", 2) : NULL;
	g_errno == E_NOEXEC ? ft_putendl_fd("command not found", 2) : NULL;
	g_errno == E_ACCESS ? ft_putendl_fd("permission denied", 2) : NULL;
	g_errno == E_2BIG ? ft_putendl_fd("too many arguments", 2) : NULL;
}
