/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: agrossma <agrossma@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/04/12 15:45:40 by agrossma          #+#    #+#             */
/*   Updated: 2018/04/19 18:33:28 by agrossma         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"

static int	ft_execute(char *bin, char **argv)
{
	pid_t	cpid;
	pid_t	w;
	int		status;

	if (access(bin, F_OK) || access(bin, X_OK))
	{
		g_errno = !access(bin, F_OK) && access(bin, X_OK) ? E_ACCESS : E_NOENT;
		ft_perror(bin);
		ft_strdel(&bin);
		return (127);
	}
	cpid = fork();
	signal(SIGINT, &ft_signal_handler_cmd);
	if (!cpid)
		(void)execve(bin, argv, g_environ);
	else
	{
		w = waitpid(cpid, &status, 0);
		if (w == -1)
			return (-1);
	}
	ft_strdel(&bin);
	return (0);
}

static int	ft_binary(char **argv)
{
	char	**path;
	char	*join;
	char	*bin;
	int		i;

	path = ft_strsplit(ft_getenv("PATH"), ':');
	i = 0;
	while (path && path[i])
	{
		join = ft_strjoin(path[i++], "/");
		bin = ft_strjoin(join, argv[0]);
		ft_strdel(&join);
		if (!access(bin, F_OK) && !access(bin, X_OK))
		{
			ft_memdel2d((void **)path);
			return (ft_execute(bin, argv));
		}
		g_errno = !access(bin, F_OK) && access(bin, X_OK) ? E_ACCESS : g_errno;
		ft_strdel(&bin);
	}
	g_errno = E_NOEXEC;
	ft_memdel2d((void **)path);
	ft_perror(argv[0]);
	return (127);
}

int			ft_builtin(char **argv)
{
	if (ft_strchr(argv[0], '/'))
		return (ft_execute(ft_strdup(argv[0]), argv));
	if (!ft_strcmp(argv[0], "cd"))
		return (ft_cd(argv));
	else if (!ft_strcmp(argv[0], "echo"))
		return (ft_echo(argv));
	else if (!ft_strcmp(argv[0], "env"))
		return (ft_env(argv));
	else if (!ft_strcmp(argv[0], "exit"))
		return (ft_exit(argv));
	else if (!ft_strcmp(argv[0], "setenv"))
		return (ft_setenv(argv));
	else if (!ft_strcmp(argv[0], "unsetenv"))
		return (ft_unsetenv(argv));
	else
		return (ft_binary(argv));
}
